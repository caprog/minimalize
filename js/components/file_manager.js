const FileManager = {
    data() {
        return {
            files: [
                {
                    name: 'prod',
                    isDirectory: true,
                    files: [
                        {
                            name: 'lake',
                            isDirectory: true,
                            files: [
                                {
                                    name: '1.parquet'
                                },
                                {
                                    name: '1.parquet'
                                },
                                {
                                    name: '1.parquet'
                                }
                            ]
                        }
                    ]
                },
                {
                    name: 'uat',
                    isDirectory: true,
                    files: [
                        {
                            name: 'lake',
                            isDirectory: true,
                            files: [
                                {
                                    name: '1.parquet'
                                },
                                {
                                    name: '1.parquet'
                                },
                                {
                                    name: '1.parquet'
                                }
                            ]
                        }
                    ]
                },
                {
                    name: 'dev',
                    isDirectory: true,
                    files: [
                        {
                            name: 'lake',
                            isDirectory: true,
                            files: [
                                {
                                    name: '1.parquet'
                                },
                                {
                                    name: '1.parquet'
                                },
                                {
                                    name: '1.parquet'
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    },
    template: `
    <ul class="file-manager">
        <template v-for="file in files">
            <li v-if="file.isDirectory"><span>&#128447;</span>{{file.name}}</li>
        </template>
    </ul>
    `
}